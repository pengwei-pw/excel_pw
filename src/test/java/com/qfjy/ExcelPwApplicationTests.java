package com.qfjy;

import com.alibaba.excel.EasyExcel;
import com.qfjy.entity.po.Uservip;
import com.qfjy.excelUtils.PageUtils;
import com.qfjy.listener.ExcelListener;
import com.qfjy.mapper.UservipMapper;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class ExcelPwApplicationTests {

    @Autowired
    private UservipMapper usermapper;
    @Test
    void contextLoads() {
        //String fileName = "D:\\new idea work space 01\\excel_pw\\weiuser.xls";
        // 这里默认读取第一个sheet
        //EasyExcel.read(fileName, Uservip.class, new ExcelListener(usermapper)).sheet().doRead();
//        List<Uservip> list= Lists.newArrayList();
//        Uservip uservip=new Uservip();
//        uservip.setCity("jsa");
//        uservip.setOpenid("fjadkljfkadsljfkladsjfkldasjfkldas");
//        list.add(uservip);
//        EasyExcel.write(fileName, Uservip.class).sheet("mmm").doWrite(list);
        PageUtils pageUtils=new PageUtils(usermapper.selectCount(),6,1);
        List<Uservip> list=usermapper.selectByPage(pageUtils);
        int ii=pageUtils.getPageSize();
        System.out.println(list);
    }

}
