package com.qfjy.controller;

import com.qfjy.bean.ResponseInfo;
import com.qfjy.bean.SearchContidion;
import com.qfjy.entity.po.ProvinceCount;
import com.qfjy.entity.po.Uservip;
import com.qfjy.excelUtils.PageUtils;
import com.qfjy.service.impl.AdminServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author pengwei
 * @Title:
 * @Description:
 * @date 2021/8/14  9:35
 */
@RequestMapping("admin")
@Controller
public class AdminController {
    @Autowired
    private AdminServiceImpl adminService;

    @RequestMapping("list")
    public String list(ModelMap modelMap){
        PageUtils pageUtils =adminService.selectBypage(1);
        modelMap.put("pageUtils",pageUtils);
        return "admin-list";
    }
    @RequestMapping("page")
    @ResponseBody
    public PageUtils list(@RequestParam(value = "currentpage" ,defaultValue = "1") String currentpage){
        PageUtils pageUtils =adminService.selectBypage(Integer.parseInt(currentpage));
        return pageUtils;
    }

    @ResponseBody
    @RequestMapping("del")
    public ResponseInfo del(HttpServletRequest request){
        String [] arr=request.getParameterValues("id");
        adminService.del(arr);
        return ResponseInfo.success();
    }


    @RequestMapping("search")
    @ResponseBody
    public ResponseInfo search(SearchContidion contidion){
        List<Uservip> list=adminService.search(contidion);
        return ResponseInfo.success(list);
    }

    @RequestMapping("see")
    public String getEcharts(ModelMap modelMap){
        List<ProvinceCount> list=adminService.selectNameAndCount();
        modelMap.put("list",list);
        return "charts";
    }
}

