package com.qfjy.mapper;


import com.qfjy.bean.SearchContidion;
import com.qfjy.entity.po.ProvinceCount;
import com.qfjy.entity.po.Uservip;
import com.qfjy.excelUtils.PageUtils;

import java.util.List;

public interface UservipMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Uservip record);

    int insertSelective(Uservip record);

    Uservip selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Uservip record);

    int updateByPrimaryKey(Uservip record);

    void save(List<Uservip> list);

    int selectCount();

    List<Uservip> selectByPage(PageUtils pageUtils);

    void deleteByArray(String [] arr);

    List<Uservip> selectByContidion(SearchContidion contidion);

    List<ProvinceCount> selectByGroup();
}