package com.qfjy.Enum;

public enum ResponseCodeEnum {
    SUCCESS("200","删除成功"),
    FAILURE("500","请输入正确的账号密码"),
    FAILURECOUNT("515","密码五次错误，20分钟后重新登录"),
    FAILUREUSERNAME("516","账号错误"),
    FAILUREPASSWORD("517","密码错误");
    private String code;
    private String msg;

    ResponseCodeEnum(String code, String msg) {

        this.code = code;
        this.msg = msg;
    }
    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
