package com.qfjy;

import com.alibaba.excel.EasyExcel;
import com.qfjy.entity.po.Uservip;
import com.qfjy.listener.ExcelListener;
import com.qfjy.mapper.UservipMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;

/**
 * @author pengwei
 * @Title:
 * @Description:
 * @date 2021/8/13  23:21
 */
public class Estest {

    @Autowired
    private UservipMapper usermapper;
    @Test
    public void indexOrNameRead() {
        String fileName = "D:\\new idea work space 01\\excel_pw\\weiuser.xls";
        // 这里默认读取第一个sheet
        EasyExcel.read(fileName, Uservip.class, new ExcelListener(usermapper)).sheet().doRead();
    }
}
