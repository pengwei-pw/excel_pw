package com.qfjy.excelUtils;

import com.alibaba.excel.EasyExcel;
import com.qfjy.entity.po.Uservip;
import com.qfjy.listener.ExcelListener;
import com.qfjy.mapper.UservipMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author pengwei
 * @Title:
 * @Description:
 * @date 2021/8/14  8:48
 */
public class ExcelUtil {


    @Autowired
    private UservipMapper usermapper;

    /**
     * 对excel写入操作
     * @param fileName
     */
    public void read(String fileName){
        EasyExcel.read(fileName, Uservip .class, new ExcelListener(usermapper)).sheet().doRead();
    }

    /**
     * 对excel写出操作
     * @param list
     */
    public void write(List list,String fileName,String sheetName){
        EasyExcel.write(fileName, Uservip.class).sheet(sheetName).doWrite(list);
    }
}
