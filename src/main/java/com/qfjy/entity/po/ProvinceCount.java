package com.qfjy.entity.po;

import lombok.Data;

import java.io.Serializable;

/**
 * @author pengwei
 * @Title:
 * @Description:
 * @date 2021/8/15  17:53
 */
@Data
public class ProvinceCount implements Serializable {
    private String province;
    private int num;
}
