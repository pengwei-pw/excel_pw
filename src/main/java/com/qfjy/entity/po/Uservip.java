package com.qfjy.entity.po;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class Uservip implements Serializable {
    @ExcelProperty("id")
    private Integer id;
    @ExcelProperty("openid")
    private String openid;
    @ExcelProperty("nickname")
    private String nickname;
    @ExcelProperty("sex")
    private String sex;
    @ExcelProperty("city")
    private String city;
    @ExcelProperty("country")
    private String country;
    @ExcelProperty("province")
    private String province;
    @ExcelProperty("headimgurl")
    private String headimgurl;
    @ExcelProperty("subscribe")
    private String subscribe;
    @ExcelProperty("language")
    private String language;
    @ExcelProperty("remark")
    private String remark;

}