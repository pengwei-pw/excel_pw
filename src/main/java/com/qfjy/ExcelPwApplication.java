package com.qfjy;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(value = "com.qfjy.mapper")
public class ExcelPwApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExcelPwApplication.class, args);
    }

}
