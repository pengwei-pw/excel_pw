package com.qfjy.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * @author pengwei
 * @Title:
 * @Description:
 * @date 2021/8/15  13:36
 */
@Data
public class SearchContidion implements Serializable {
    private String nickname;
    private String datemax;
    private String datemin;
}
