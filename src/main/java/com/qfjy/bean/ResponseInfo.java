package com.qfjy.bean;

import com.qfjy.Enum.ResponseCodeEnum;
import lombok.Data;

/**
 * @author pengwei
 * @Title:
 * @Description:
 * @date 2021/8/6  18:02
 */

@Data
public class ResponseInfo {
    /**
     * 响应状态码，
     */
    private String code;
    /**
     * 响应描述信息
     */
    private String msg;
    /**
     * 响应数据
     */
    private Object obj;

    public ResponseInfo() {
    }

    public ResponseInfo(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ResponseInfo(String code, String msg, Object obj) {
        this.code = code;
        this.msg = msg;
        this.obj = obj;
    }

    public static ResponseInfo success() {
        return new ResponseInfo(ResponseCodeEnum.SUCCESS.getCode(), ResponseCodeEnum.SUCCESS.getMsg());
    }

    public static ResponseInfo success(Object obj) {
        return new ResponseInfo(ResponseCodeEnum.SUCCESS.getCode(), ResponseCodeEnum.SUCCESS.getMsg(), obj);
    }

    public static ResponseInfo failure() {
        return new ResponseInfo(ResponseCodeEnum.FAILURE.getCode(), ResponseCodeEnum.FAILURE.getMsg());
    }
    public static ResponseInfo failureCount() {
        return new ResponseInfo(ResponseCodeEnum.FAILURECOUNT.getCode(), ResponseCodeEnum.FAILURECOUNT.getMsg());
    }
    public static ResponseInfo failureUsername() {
        return new ResponseInfo(ResponseCodeEnum.FAILUREUSERNAME.getCode(), ResponseCodeEnum.FAILUREUSERNAME.getMsg());
    }
    public static ResponseInfo failurePassword() {
        return new ResponseInfo(ResponseCodeEnum.FAILUREPASSWORD.getCode(), ResponseCodeEnum.FAILUREPASSWORD.getMsg());
    }
}