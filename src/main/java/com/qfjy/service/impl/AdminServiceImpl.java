package com.qfjy.service.impl;

import cn.hutool.core.convert.Convert;
import com.qfjy.bean.SearchContidion;
import com.qfjy.entity.po.ProvinceCount;
import com.qfjy.entity.po.Uservip;
import com.qfjy.excelUtils.PageUtils;
import com.qfjy.mapper.UservipMapper;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author pengwei
 * @Title:
 * @Description:
 * @date 2021/8/14  9:37
 */
@Service
public class AdminServiceImpl {
    @Autowired
    private UservipMapper uservipMapper;

    public PageUtils selectBypage(Integer currentpage) {
        PageUtils pageUtils=new PageUtils(uservipMapper.selectCount(),6,currentpage);
        List<Uservip> list=uservipMapper.selectByPage(pageUtils);
        pageUtils.setList(list);
        return pageUtils;
    }

    public void del(String[] arr) {
        uservipMapper.deleteByArray(arr);
    }

    public List<Uservip> search(SearchContidion contidion) {
        return uservipMapper.selectByContidion(contidion);
    }

    public List<ProvinceCount> selectNameAndCount() {
        List<ProvinceCount> list=uservipMapper.selectByGroup();
        return list;
    }
}
